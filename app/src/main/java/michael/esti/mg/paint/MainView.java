package michael.esti.mg.paint;

import android.content.Context;
import android.graphics.Canvas;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.DragEvent;
import android.view.MotionEvent;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import michael.esti.mg.models.Line;
import michael.esti.mg.models.Oval;
import michael.esti.mg.models.Rectangle;
import michael.esti.mg.models.Shape;

public class MainView extends View {


    private float x = 200f;
    private float y = 200f;
    private static final float sizeCenter = 100f;
    private float xEnd = 200f;
    private float yEnd = 200f;
    Boolean isOnlymove = false;
    private Boolean isSelection = false;

    float distanceX = 0f;
    float distanceY = 0f;
    float distanceXBas = 0f;
    float distanceYBas = 0f;

    private Mode modeImage = Mode.RECTANGLE;
    List<Shape> listFigures = new ArrayList<>();
    private static Shape currentObjSelected = null;





    public MainView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onDraw(Canvas canvas) {

        for (Shape shape : listFigures) {

                shape.drawShape(canvas);

        }
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                x = event.getX();
                y = event.getY();
                for (Shape item : listFigures) {
                    if (item.isSelected(x, y)) {
                        distanceX = x - item.getStartX();
                        distanceY = y - item.getStartY();
                        distanceXBas = item.getStopX() - x;
                        distanceYBas = item.getStopY() - y;
                        isSelection = true;
                        currentObjSelected = item;
                    }
                }
                break;
            case MotionEvent.ACTION_MOVE:
                if (isSelection) {
                    x = event.getX();
                    y = event.getY();
                    Shape shape = currentObjSelected;
                    shape.setStartX((int) x - (int) distanceX);
                    shape.setStartY((int) y - (int) distanceY);
                    shape.setStopX((int) x + (int) distanceXBas);
                    shape.setStopY((int) y + (int) distanceYBas);
                    currentObjSelected = shape;
                } else {
                    xEnd = event.getX();
                    yEnd = event.getY();
                }
                break;
            case MotionEvent.ACTION_UP:
                if (!isSelection) {
                    if (modeImage.equals(Mode.RECTANGLE)) {
                        Rectangle rectangle = new Rectangle((int) x, (int) y, (int) xEnd, (int) yEnd);
                        listFigures.add(rectangle);
                    } else if (modeImage.equals(Mode.CERCLE)) {
                        Oval oval = new Oval((int) x, (int) y, (int) xEnd, (int) yEnd);
                        listFigures.add(oval);
                    } else {
                        Line l = new Line((int) x, (int) y, (int) xEnd, (int) yEnd);
                        listFigures.add(l);
                    }
                }
                isSelection = false;
                break;
        }

        invalidate();
        return true;
    }

    public double calculateDistanceBetweenPoints(
            float x1,
            float y1,
            float x2,
            float y2) {
        return Math.sqrt((y2 - y1) * (y2 - y1) + (x2 - x1) * (x2 - x1));
    }

    public Mode getModeImage() {
        return modeImage;
    }

    public void setModeImage(Mode modeImage) {
        this.modeImage = modeImage;
    }

    @Override
    public boolean onDragEvent(DragEvent event) {
        return super.onDragEvent(event);
    }
}
