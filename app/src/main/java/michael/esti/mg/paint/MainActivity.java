package michael.esti.mg.paint;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;

import michael.esti.mg.SessionColor;
import yuku.ambilwarna.AmbilWarnaDialog;

public class MainActivity extends AppCompatActivity {

    public ImageButton cercle;
    public ImageButton rectangle;
    public ImageButton line;
    public ImageButton color;


    public MainView mainView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
        final AmbilWarnaDialog dialog = new AmbilWarnaDialog(this, 0xff000000, new AmbilWarnaDialog.OnAmbilWarnaListener() {
            @Override
            public void onOk(AmbilWarnaDialog dialog, int color) {
                if (SessionColor.currentMode.equals(Mode.CERCLE)) {

                    SessionColor.CurrentColorOval = color;

                } else if (SessionColor.currentMode.equals(Mode.RECTANGLE)){
                    SessionColor.CurrentColorRectangle = color;
                }
                else {
                    SessionColor.CurrentColorLine = color;
                }
            }

            @Override
            public void onCancel(AmbilWarnaDialog dialog) {

            }
        });

        cercle = findViewById(R.id.image_button_cercle);
        rectangle = findViewById(R.id.image_button_rectange);
        line = findViewById(R.id.image_button_ligne);
        mainView = findViewById(R.id.mainview);
        color = findViewById(R.id.image_button_color);
        cercle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainView.setModeImage(Mode.CERCLE);
                SessionColor.currentMode = Mode.CERCLE;
            }
        });
        rectangle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainView.setModeImage(Mode.RECTANGLE);
                SessionColor.currentMode = Mode.RECTANGLE;
            }
        });
        line.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainView.setModeImage(Mode.LIGNE);
                SessionColor.currentMode = Mode.LIGNE;
            }
        });

        color.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.show();
            }
        });

    }
}
