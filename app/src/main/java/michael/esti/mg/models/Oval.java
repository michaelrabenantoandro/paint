package michael.esti.mg.models;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;

import michael.esti.mg.SessionColor;

public class Oval extends Shape {


    public Oval(float startX, float startY, float stopX, float stopY) {
        super(startX, startY, stopX, stopY);
    }

    @Override
    public void drawShape(Canvas c) {
        Paint p = new Paint();
        p.setColor(SessionColor.CurrentColorOval);
        RectF rectF = new RectF();
        rectF.set((int) getStartX(), (int) getStartY(), (int) getStopX(), (int) getStopY());
        c.drawOval(rectF, p);
    }


}
