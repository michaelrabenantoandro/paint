package michael.esti.mg.models;

import android.graphics.Canvas;
import android.graphics.Paint;

import michael.esti.mg.SessionColor;

public class Line  extends Shape{


    public Line() {
    }

    public Line(float startX, float startY, float stopX, float stopY) {
        super(startX, startY, stopX, stopY);
    }

    @Override
    public void drawShape(Canvas c) {
        Paint p = new Paint();
        p.setColor(SessionColor.CurrentColorLine);
        p.setStrokeWidth(5f);
        Line l = new Line((int) getStartX(), (int) getStartY(), (int) getStopX(), (int) getStopY());
        c.drawLine(l.getStartX(), l.getStartY(), l.getStopX(), l.getStopY(), p);
    }


}
