package michael.esti.mg.models;

import android.graphics.Canvas;

public abstract class Shape {
    private float startX;
    private float startY;
    private float stopX;
    private float stopY;


    public Shape() {
    }

    public Shape(float startX, float startY, float stopX, float stopY) {
        this.startX = startX;
        this.startY = startY;
        this.stopX = stopX;
        this.stopY = stopY;
    }

    public abstract void drawShape(Canvas c);

    public boolean isSelected(float x, float y) {
        if (x > startX && x < stopX && y > startY && y < stopY) {
            return true;
        }
        return false;
    }

    public float getStartX() {
        return startX;
    }

    public void setStartX(float startX) {
        this.startX = startX;
    }

    public float getStartY() {
        return startY;
    }

    public void setStartY(float startY) {
        this.startY = startY;
    }

    public float getStopX() {
        return stopX;
    }

    public void setStopX(float stopX) {
        this.stopX = stopX;
    }

    public float getStopY() {
        return stopY;
    }

    public void setStopY(float stopY) {
        this.stopY = stopY;
    }
}
