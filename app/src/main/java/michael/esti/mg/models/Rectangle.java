package michael.esti.mg.models;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

import michael.esti.mg.SessionColor;

public class Rectangle extends  Shape {


    public Rectangle(float startX, float startY, float stopX, float stopY) {
        super(startX, startY, stopX, stopY);

    }

    @Override
    public void drawShape(Canvas c) {
        Paint p = new Paint();
        p.setColor(SessionColor.CurrentColorRectangle);
        Rect r = new Rect();
        r.set((int) getStartX(), (int) getStartY(), (int) getStopX(), (int) getStopY());
        c.drawRect(r, p);

    }


}
