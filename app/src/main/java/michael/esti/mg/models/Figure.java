package michael.esti.mg.models;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

import java.util.List;

public abstract class Figure {
    public abstract void draw(Paint paint, Canvas canvas ,List<Object> objects, Object currentObject);

}
