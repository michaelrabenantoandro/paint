package michael.esti.mg.utils;

public class DrawUtils {
    public double calculateDistanceBetweenPoints(
            float x1,
            float y1,
            float x2,
            float y2) {
        return Math.sqrt((y2 - y1) * (y2 - y1) + (x2 - x1) * (x2 - x1));
    }
}
